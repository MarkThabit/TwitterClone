//
//  AppDelegate.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/3/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import TwitterKit
import Reachability

var reachability: Reachability!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{    
    // MARK: - iVars
    
    var window: UIWindow?

    // MARK: - Application life cycle
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        reachability = Reachability()
        createLocalDataFile()
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: Constants.kConsumerKey,
                                           consumerSecret: Constants.kConsumerSecretKey)
        return true
    }
    
    // To handle the log in redirect back to your app.
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool
    {
        return TWTRTwitter.sharedInstance().application(app,
                                                        open: url,
                                                        options: options)
    }
    
    private func createLocalDataFile()
    {
        if let usersURL = getLocalDataURL(),
            FileManager.default.fileExists(atPath: usersURL.path) == false
        {            
            NSKeyedArchiver.archiveRootObject([User](), toFile: usersURL.path)
        }
    }
}
