//
//  ImageDetailsVC.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/8/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import SDWebImage

protocol ImageDetailsVCDelegaate
{
    func imageDetailsDidFinished()
}

class ImageDetailsVC: UIViewController
{
    // MARK: - IBOutlets
    
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - iVars
    
    var image: UIImage?
    var delegate: ImageDetailsVCDelegaate!
    
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.imageView.image = self.image
    }
    
    // MARK: - Target Action
    
    @IBAction func clossThisVC(_ sender: UIButton)
    {
        self.delegate.imageDetailsDidFinished()
    }
}

// MARK: - UIScrollViewDelegate

extension ImageDetailsVC: UIScrollViewDelegate
{
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.imageView
    }
}
