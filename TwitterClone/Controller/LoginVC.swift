//
//  LoginVC.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/3/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import TwitterKit

class LoginVC: UIViewController
{
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // MARK: - Target Actions
    
    @IBAction func loginButtonTapped(_ sender: UIButton)
    {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            
            if let session = session
            {
                print("signed in");
                
                self.saveUserCredentials(fromSession: session)
                
                // Go to user followers view controller
                self.performSegue(withIdentifier: "UserFollowersVC",
                                  sender: nil)
            }
            else
            {
                print("error: \(error?.localizedDescription ?? "")");
            }
        }
    }
    
    // MARK: - Private Methods
    
    /// Save user credentials in userDefaults
    ///
    /// - Parameter session: Twitter session that has user data
    private func saveUserCredentials(fromSession session: TWTRSession)
    {
        let userDefault = UserDefaults.standard
        
        userDefault.set(session.userID, forKey: Constants.kUserID)
        userDefault.set(session.userName, forKey: Constants.kUserName)
        userDefault.set(session.authToken, forKey: Constants.kAuthToken)
        userDefault.set(session.authTokenSecret, forKey: Constants.kAuthTokenSecret)
        
        userDefault.synchronize()
    }
}
