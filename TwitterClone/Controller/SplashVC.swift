//
//  SplashVC.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/3/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

class SplashVC: UIViewController
{
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // User already loogin in
        if let _ = UserDefaults.standard.string(forKey: Constants.kUserID)
        {
            self.performSegue(withIdentifier: "UserFollowersVC", sender: nil)
        }
        else
        {
            self.performSegue(withIdentifier: "LoginVC", sender: nil)
        }
    }
}
