//
//  UserFollowerInfoVC.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/3/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import SDWebImage

class UserFollowerInfoVC: UIViewController
{
    // MARK: - iVars
    
    var currentUser: User!
    var tweets = [Tweet]()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userHandleLbl: UILabel!
    
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.profileImageView.addGestureRecognizer(createTapGesture())
        self.backgroundImageView.addGestureRecognizer(createTapGesture())
        
        if let bgURL = self.currentUser.backgroundImageURL
        {
            self.backgroundImageView.sd_setImage(with: URL(string: bgURL),
                                                 placeholderImage: UIImage(named: "bg"))
        }
        else // Doesn't use background image => use default image
        {
            self.backgroundImageView.image = UIImage(named: "bg")
        }
        
        self.profileImageView.sd_setImage(with: URL(string: self.currentUser.profileImageURL!),
                                          placeholderImage: UIImage(named: "placeholder"))
        
        self.userNameLbl.text = self.currentUser.name
        self.userHandleLbl.text = "@\(self.currentUser.handle!)"
        
        self.profileImageView.layer.borderWidth = 3
        self.profileImageView.layer.borderColor = UIColor.white.cgColor
        self.profileImageView.layer.cornerRadius = 3
        self.profileImageView.clipsToBounds = true
        
        self.getLastTenTweets()
    }
    
    // MARK: - Private Methods
    
    private func getLastTenTweets()
    {
        HTTPRequest.requestUserTweets(userId: self.currentUser.id!,
                                      successBlock: { tweets in
                                        
                                        self.tweets = tweets
                                        self.tableView.reloadData()
        }) { error in
            
            let alert = UIAlertController(title: "Error",
                                          message: error.localizedDescription,
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "O.K.",
                                         style: .default,
                                         handler: nil)
            
            alert.addAction(okAction)
            
            self.present(alert,
                         animated: true,
                         completion: nil)
        }
    }
    
    private func createTapGesture() -> UITapGestureRecognizer
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        
        return tapGesture
    }
    
    // MARK: - Target Actions
    
    @IBAction func imageTapped(_ gesture: UITapGestureRecognizer)
    {
        let imageView = gesture.view as! UIImageView
        
        self.performSegue(withIdentifier: "ImageDetailsVC",
                          sender: imageView.image)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ImageDetailsVC",
            let imageDetailsVC = segue.destination as? ImageDetailsVC
        {
            imageDetailsVC.image = sender as? UIImage
            imageDetailsVC.delegate = self
        }
    }
}

// MARK: - UITableViewDataSource

extension UserFollowerInfoVC: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "TweetCell", for: indexPath) as! TweetCell
        
        cell.configureCell(tweet: self.tweets[indexPath.row])
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension UserFollowerInfoVC: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return self.headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 128
    }
}

extension UserFollowerInfoVC: ImageDetailsVCDelegaate
{
    func imageDetailsDidFinished()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
