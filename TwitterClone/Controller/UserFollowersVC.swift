//
//  UserFollowersVC.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/3/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

class UserFollowersVC: UIViewController
{
    // MARK: - Private iVars
    
    private var followers = [User]()
    private lazy var refreshControl: UIRefreshControl = { [weak self] in
        let refresh = UIRefreshControl()
        refresh.addTarget(self,
                          action: #selector(self?.refreshAction(_:)),
                          for: .valueChanged)
        return refresh
    }()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableView.refreshControl = self.refreshControl
        
        HTTPRequest.requestUserFollowers(pageIndex: "-1",
                                         successBlock: { [weak self] users in
                                            
                                            guard let `self` = self else { return }
                                            
                                            self.followers.insert(contentsOf: users, at: 0)
                                            self.tableView.reloadData()
        }) { [weak self] error in
            
            guard let `self` = self else { return }
            
            // Get cached data from plist file
            if let localDataURL = getLocalDataURL()
            {
                self.followers = NSKeyedUnarchiver.unarchiveObject(withFile: localDataURL.path) as! [User]
                self.tableView.reloadData()
            }
            
            // Present a prompt to user
            self.navigationItem.prompt = "You still can view data in offline mode"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.navigationItem.prompt = nil
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true
    }
    
    // MARK: - Target Aciton
    
    @IBAction func refreshAction(_ sender: UIRefreshControl)
    {
        if reachability.connection != .none
        {
            if let nextPage = UserDefaults.standard.string(forKey: Constants.kNextPage),
                nextPage != "0"
            {
                HTTPRequest.requestUserFollowers(pageIndex: nextPage,
                                                 successBlock: { [weak self] users in
                                                    
                                                    guard let `self` = self else { return }
                                                    
                                                    if (self.tableView.refreshControl?.isRefreshing)!
                                                    {
                                                        self.tableView.refreshControl?.endRefreshing()
                                                    }
                                                    
                                                    self.followers.insert(contentsOf: users, at: 0)
                                                    self.tableView.reloadData()
                }) { [weak self] error in
                    
                    guard let `self` = self else { return }
                    
                    if (self.tableView.refreshControl?.isRefreshing)!
                    {
                        self.tableView.refreshControl?.endRefreshing()
                    }
                    
                    // Get cached data from plist file
                    if let localDataURL = getLocalDataURL()
                    {
                        self.followers = NSKeyedUnarchiver.unarchiveObject(withFile: localDataURL.path) as! [User]
                        self.tableView.reloadData()
                    }
                }
            }
            else
            {
                let alert = UIAlertController(title: "Info",
                                              message: "No more data found",
                                              preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "O.K.",
                                             style: .default,
                                             handler: { alert in
                                                
                                                if (self.tableView.refreshControl?.isRefreshing)!
                                                {
                                                    self.tableView.refreshControl?.endRefreshing()
                                                }
                })
                
                alert.addAction(okAction)
                
                self.present(alert,
                             animated: true,
                             completion: nil)
            }
        }
        else
        {
            if (self.tableView.refreshControl?.isRefreshing)!
            {
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "UserFollowerInfoVC",
            let userFollowerInfoVC = segue.destination as? UserFollowerInfoVC,
            let indexPath = self.tableView.indexPathForSelectedRow
        {
            let selectedUser = self.followers[indexPath.row]
            userFollowerInfoVC.currentUser = selectedUser
        }
    }
}

// MARK: - UITableViewDataSource

extension UserFollowersVC: UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.followers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "followerCell", for: indexPath) as! FollowerCell
        
        cell.configureCell(withUser: self.followers[indexPath.row])
        
        return cell
    }
}

