//
//  Tweet.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/8/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation

class Tweet
{
    var id: String?
    var text: String?
    
    init(withData data: [String: Any])
    {
        if let id = data[Constants.TweetKeys.kId] as? String,
            let text = data[Constants.TweetKeys.kText] as? String
        {
            self.id = id
            self.text = text
        }
    }
    
    class func parseTweets(fromData data: [[String: Any]]) -> [Tweet]
    {
        var items = [Tweet]()
        
        for tweetData in data
        {
            items.append(Tweet(withData: tweetData))
        }
        
        return items
    }
}
