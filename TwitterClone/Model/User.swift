//
//  User.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/6/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding
{
    var id: String?
    var name: String?
    var handle: String?
    var bio: String?
    var profileImageURL: String?
    var backgroundImageURL: String?
    
    init(withData data: [String: Any])
    {
        if let id = data[Constants.UserKeys.kId] as? String,
            let name = data[Constants.UserKeys.kName] as? String,
            let handle = data[Constants.UserKeys.kHandle] as? String,
            let profileImageURL = data[Constants.UserKeys.kProfileImageURL] as? String
        {
            self.id = id
            self.name = name
            self.handle = handle
            self.profileImageURL = profileImageURL
        }
        
        if let backgroundImageURL = data[Constants.UserKeys.kBackgroundImageURL] as? String
        {
            self.backgroundImageURL = backgroundImageURL
        }
        
        if let bio = data[Constants.UserKeys.kBio] as? String
        {
            self.bio = bio
        }
    }
    
    class func parseUsers(fromData data: [String: Any]) -> [User]
    {
        var items = [User]()
        
        if let users = data[Constants.UserKeys.kUserList] as? [[String: Any]]
        {
            for userData in users
            {
                items.append(User(withData: userData))
            }
        }
        
        self.cacheData(data: items)
        
        return items
    }
    
    // MARK: - Private Methods
    
    private class func cacheData(data: [User])
    {
        if let localDataURL = getLocalDataURL()
        {
            NSKeyedArchiver.archiveRootObject(data, toFile: localDataURL.path)
        }
    }
    
    // MARK: - NSCoding
    
    required init?(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: Constants.UserKeys.kId) as? String
        name = aDecoder.decodeObject(forKey: Constants.UserKeys.kName) as? String
        handle = aDecoder.decodeObject(forKey: Constants.UserKeys.kHandle) as? String
        bio = aDecoder.decodeObject(forKey: Constants.UserKeys.kBio) as? String
        profileImageURL = aDecoder.decodeObject(forKey: Constants.UserKeys.kProfileImageURL) as? String
        backgroundImageURL = aDecoder.decodeObject(forKey: Constants.UserKeys.kBackgroundImageURL) as? String
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.id, forKey: Constants.UserKeys.kId)
        aCoder.encode(self.name, forKey: Constants.UserKeys.kName)
        aCoder.encode(self.handle, forKey: Constants.UserKeys.kHandle)
        aCoder.encode(self.bio, forKey: Constants.UserKeys.kBio)
        aCoder.encode(self.profileImageURL, forKey: Constants.UserKeys.kProfileImageURL)
        aCoder.encode(self.backgroundImageURL, forKey: Constants.UserKeys.kBackgroundImageURL)
    }
}
