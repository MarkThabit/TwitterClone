//
//  HTTPRequest.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/6/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation
import TwitterKit

struct HTTPRequest
{
    static func requestUserFollowers(pageIndex: String,
                                     successBlock: @escaping ([User]) -> Void,
                                     failureBlock: @escaping (Error) -> Void)
    {
        if reachability.connection != .none
        {
            let client = TWTRAPIClient()
            let endpoint = "https://api.twitter.com/1.1/followers/list.json"
            let params: [String: Any] = ["user_id": UserDefaults.standard.string(forKey: Constants.kUserID)!,
                                         "cursor": pageIndex,
                                         "count": Constants.kPageSize]
            var clientError: NSError?
            
            let request = client.urlRequest(withMethod: "GET",
                                            urlString: endpoint,
                                            parameters: params,
                                            error: &clientError)
            
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                
                if let connectionError = connectionError
                {
                    print("Error: \(connectionError)")
                    failureBlock(connectionError)
                }
                
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: Any]
                    print(json)
                    let followers = User.parseUsers(fromData: json)
                    
                    successBlock(followers)
                    
                    if let nextPage = json[Constants.kNextPage] as? String
                    {
                        UserDefaults.standard.set(nextPage, forKey: Constants.kNextPage)
                        UserDefaults.standard.synchronize()
                    }
                    else
                    {
                        UserDefaults.standard.set(nil, forKey: Constants.kNextPage)
                        UserDefaults.standard.synchronize()
                    }
                    
                    print(followers)
                }
                catch let jsonError as NSError
                {
                    print("json error: \(jsonError.localizedDescription)")
                }
            }
        }
        else
        {
            let error = NSError(domain: "Network",
                                code: 0,
                                userInfo: [NSLocalizedDescriptionKey: "Check your connection"])
            
            failureBlock(error)
        }
    }
    
    static func requestUserTweets(userId: String,
                                  successBlock: @escaping ([Tweet]) -> Void,
                                  failureBlock: @escaping (Error) -> Void)
    {
        if reachability.connection != .none
        {
            let client = TWTRAPIClient()
            let endpoint = "https://api.twitter.com/1.1/statuses/user_timeline.json"
            let params: [String: Any] = ["user_id": userId,
                                         "count": Constants.kPageSize,
                                         "trim_user": "true"]
            var clientError: NSError?
            
            let request = client.urlRequest(withMethod: "GET",
                                            urlString: endpoint,
                                            parameters: params,
                                            error: &clientError)
            
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                
                if let connectionError = connectionError
                {
                    print("Error: \(connectionError)")
                    failureBlock(connectionError)
                }
                
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String: Any]]
                    print(json)
                    
                    let tweets = Tweet.parseTweets(fromData: json)
                    
                    successBlock(tweets)
                }
                catch let jsonError as NSError
                {
                    print("json error: \(jsonError.localizedDescription)")
                }
            }
        }
        else
        {
            let error = NSError(domain: "Network",
                                code: 0,
                                userInfo: [NSLocalizedDescriptionKey: "Check your connection"])
            
            failureBlock(error)
        }
    }
}
