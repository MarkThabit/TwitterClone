//
//  Constants.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/3/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation

struct Constants
{
    static let kConsumerKey       = "l0pL7i7v4yTaazAXDAhDrqIXM"
    static let kConsumerSecretKey = "I8CEU0ICWI2AN1e1qmLmtLIKr9U0Ct3LdDVuvidB8umqFyaI2s"
    
    static let kPageSize = "10"
    
    static let kUserID          = "userId"
    static let kUserName        = "userName"
    static let kAuthToken       = "authToken"
    static let kAuthTokenSecret = "authTokenSecret"
    
    static let kNextPage = "next_cursor_str"
    
    struct UserKeys
    {
        static let kId                 = "id_str"
        static let kName               = "name"
        static let kHandle             = "screen_name"
        static let kBio                = "description"
        static let kProfileImageURL    = "profile_image_url_https"
        static let kBackgroundImageURL = "profile_banner_url"
        static let kUserList           = "users"
    }
    
    struct TweetKeys
    {
        static let kId   = "id_str"
        static let kText = "text"
    }
}
