//
//  Utilities.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/7/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import Foundation

func getLocalDataURL() -> URL?
{
    do
    {
        let documentURL = try FileManager.default.url(for: .documentDirectory,
                                                      in: .userDomainMask,
                                                      appropriateFor: nil,
                                                      create: false)
        
        let usersURL = documentURL.appendingPathComponent("users.plist")
        
        return usersURL
    }
    catch
    {
        return nil
    }
}
