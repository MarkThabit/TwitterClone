//
//  FollowerCell.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/7/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit
import SDWebImage

class FollowerCell: UITableViewCell
{
    // MARK: - IBOutlets
    
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userHandleLbl: UILabel!
    @IBOutlet weak var bioLbl: UILabel!
    
    // MARK: - Actions
    
    func configureCell(withUser user: User)
    {
        if let urlString = user.profileImageURL
        {
            userProfileImage.sd_setImage(with: URL(string: urlString),
                                         placeholderImage: UIImage(named: "placeholder"))
        }
        else
        {
            userProfileImage.image = UIImage(named: "placeholder")
        }
        
        self.userNameLbl.text = user.name
        self.userHandleLbl.text = "@\(user.handle!)"
        
        if let bio = user.bio
        {
            self.bioLbl.text = bio
        }
    }
}
