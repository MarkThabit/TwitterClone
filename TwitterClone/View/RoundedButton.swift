//
//  RoundedButton.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/4/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton
{
    @IBInspectable var cornerRadius: CGFloat = 10.0
    @IBInspectable var borderWidth: CGFloat = 1
    @IBInspectable var borderColor: UIColor = UIColor.blue
        
    override func prepareForInterfaceBuilder()
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}
