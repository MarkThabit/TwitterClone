//
//  RoundedImageView.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/7/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.bounds.width / 2
        self.clipsToBounds = true
    }
}
