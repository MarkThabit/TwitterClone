//
//  TweetCell.swift
//  TwitterClone
//
//  Created by Mark Maged on 2/8/18.
//  Copyright © 2018 Mark Maged. All rights reserved.
//

import UIKit

class TweetCell: UITableViewCell
{
    // MARK: - IBOutlets
    
    @IBOutlet weak var tweetTextLbl: UILabel!
    
    // MARK: - Actions
    
    func configureCell(tweet: Tweet)
    {
        self.tweetTextLbl.text = tweet.text
    }
}
